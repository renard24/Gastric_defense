from cocos.director import director
from cocos.scene import Scene
from cocos.layer import Layer, ColorLayer
from cocos.sprite import Sprite
from virus import Virus
from tower import Tour
from cocos.text import Label
from pyglet import resource
from pyglet.window import key
import sys
sys.path.append("./Layers")
sys.path.append("./Model")
from rond import Rond
from chemin import Chemin, chargerChemin, chargerPlaces
from cocos.actions.instant_actions import Hide
from cocos.actions.instant_actions import Show
from cocos.actions.instant_actions import Place
from cocos.actions import FadeTo
import time as t
import math
import menu
import random as r
import pyglet.window

LIMITE_VAGUE=[0,10,20,30,40]

class GameLayer(Layer):

    is_event_handler = True

    def __init__(self,liste_tour,liste_emplacement,liste_virus,chemin,vie,lvl):
        super(GameLayer, self).__init__()

        self.window_w, self.window_h = pyglet.window.Window.get_size(director.window)
        self.start=t.time()
        self.chrono=30
        self.lvl = lvl
        self.vague=0
        self.virus_spawned=0
        self.schedule_interval(self.spawn_virus,2)
        self.liste_tour=liste_tour
        self.liste_emplacement=liste_emplacement
        self.liste_virus=liste_virus
        self.l=len(self.liste_emplacement)
        self.chemin=chemin
        self.depart=chemin.ronds[0]
        self.vie=vie
        self.nb_update=0
        self.liste_vie=[]
        self.selected=None
        self.text_vague=""
        self.bouton1=Sprite("assets/images/bouton_bleu.png",(600, 50),scale=0.1)
        self.bouton2=Sprite("assets/images/bouton_jaune.png",(600, 100),scale=0.1)
        self.bouton1_selected=Sprite("assets/images/bouton_bleu_selectionne.png",(600, 50),scale=0.1)
        self.bouton2_selected=Sprite("assets/images/bouton_jaune_selectionne.png",(600, 100),scale=0.1)

    def changer_vague(self):
        self.unschedule(self.spawn_virus)
        self.vague+=1
        self.virus_spawned=0
        self.nb_update+=3
        self.text_vague=Label("Vague "+str(self.vague),(225, 275),font_size = 35)
        self.add(self.text_vague)
        if self.vague>1:
            self.rechargerVie()
            self.vie+=1
            if self.vie>3:
                self.vie=3
        self.schedule_interval(self.spawn_virus,4-self.vague)


    def spawn_virus(self,dt):

        def victory(dt):
            if not(self.children_names["victory_background"].are_actions_running()):
                self.unschedule(victory)
                self.add(menu.Victory(self.lvl))

        if LIMITE_VAGUE[self.vague]>self.virus_spawned:
            if self.virus_spawned==0:
                self.remove(self.text_vague)
            special=r.random()
            if special<=0.05*self.vague:
                virus=Virus(resource.animation("assets/images/virus2.gif"),200,self.depart,2+self.vague,self.liste_tour,self)
            elif special<=0.1*self.vague and special>0.05*self.vague:
                virus=Virus(resource.animation("assets/images/virus3.gif"),100,self.depart,4+2*self.vague,self.liste_tour,self)
            else:
                virus=Virus(resource.animation("assets/images/virus1.gif"),100,self.depart,2+self.vague,self.liste_tour,self)
            self.liste_virus.append(virus)
            self.add(virus)
            self.virus_spawned+=1
        else:
            if self.liste_virus==[]:
                if self.vague<=3:
                    self.changer_vague()
                elif self.vague==4:
                    self.unschedule(self.spawn_virus)
                    victory_background = ColorLayer(255, 255, 255, 0)
                    self.add(victory_background, name = "victory_background")
                    victory_background.do(FadeTo(60, 2))
                    self.schedule(victory)

    def on_mouse_motion(self,x,y,dx,dy):
        if not("pause_effect" in self.children_names) and self.nb_update>0:
            x,y = director.get_virtual_coordinates(x,y)
            for i in range(self.l):
                if x>self.liste_emplacement[i][0].position[0]-32 and x<self.liste_emplacement[i][0].position[0]+32 and y>self.liste_emplacement[i][0].position[1]-32 and y<self.liste_emplacement[i][0].position[1]+32:
                    #si le curseur passe sur un emplacement
                    if self.liste_emplacement[i][1]<4:
                        #si la tour n'est pas completement ameliore
                        self.liste_emplacement[i][0].do(Show())
                elif x<625 and x>575 and y<60 and y>40and self.selected!=None:
                    #si le curseur passe sur le bouton de potee
                    self.bouton1_selected.do(Show())
                elif x<625 and x>575 and y<110and y>90 and self.selected!=None:
                    #si le curseur passe sur le bouton de puissance
                    self.bouton2_selected.do(Show())
                else:
                    self.bouton1_selected.do(Hide())
                    self.bouton2_selected.do(Hide())
                    if self.liste_emplacement[i][2]==0:
                        self.liste_emplacement[i][0].do(Hide())

    def on_mouse_press(self,x,y,buttons, modifiers):
        if not("pause_effect" in self.children_names) and self.nb_update>0:
            x,y = director.get_virtual_coordinates(x,y)
            for i in range(self.l):
                if x>self.liste_emplacement[i][0].position[0]-32 and x<self.liste_emplacement[i][0].position[0]+32 and y>self.liste_emplacement[i][0].position[1]- 32and y<self.liste_emplacement[i][0].position[1]+32:
                    if self.liste_emplacement[i][1]==0:
                        #si aucune tour n'est presente
                        self.add_tower(i)
                    elif self.liste_emplacement[i][1]>=1 and self.liste_emplacement[i][1]<4:
                        #si une tour est presente et n'est pas augmentee au max
                        self.selected=i
                        self.liste_emplacement[i][0].do(Show())
                        self.liste_emplacement[i][2]=1
                        self.bouton1.do(Show())
                        self.bouton2.do(Show())
                elif x<625 and x>575 and y<60 and y>40 and self.selected!=None:
                    #si clique sur le boton d'augmentation de portee
                    self.bouton1.do(Hide())
                    self.bouton2.do(Hide())
                    self.bouton1_selected.do(Hide())
                    self.update_range(self.selected)
                elif x<625 and x>575 and y<110 and y>90 and self.selected!=None:
                    #si clique sur le boton d'augmentation de puissance
                    self.bouton1.do(Hide())
                    self.bouton2.do(Hide())
                    self.bouton2_selected.do(Hide())
                    self.update_power(self.selected)
                else:
                    #si en dehors d'une tour ou d'un bouton
                    if self.liste_emplacement[i][2]==1:
                        self.selected=None
                        self.liste_emplacement[i][2]=0
                        self.liste_emplacement[i][0].do(Hide())
                        self.bouton1.do(Hide())
                        self.bouton2.do(Hide())

    def add_tower(self,i):
        #on ajoute la tour au layer et on change l'image de l'emplacement corrspondant
        tour=Tour("assets/images/medicament_vierge.png",1,150,1.5,self.liste_emplacement[i][0].position[0],self.liste_emplacement[i][0].position[1],0,0,self)
        self.liste_tour.append(tour)
        self.add(tour)
        self.remove(self.liste_emplacement[i][0])
        self.liste_emplacement[i]=[Sprite("assets/images/medicament_selectionne.png",(self.liste_emplacement[i][0].position[0],self.liste_emplacement[i][0].position[1])),1,0,tour]
        self.add(self.liste_emplacement[i][0])
        self.nb_update-=1
        self.liste_emplacement[i][0].do(Hide())

    def update_power(self,i):
        #on ajoute une tour plus puissante en supprimant l'ancienne
        tour=self.liste_emplacement[i][3]
        if self.liste_emplacement[i][1]==1:
            tour2=Tour("assets/images/medicament_pow1.png",1+(tour.puissance),(tour.rayon),1,self.liste_emplacement[i][0].position[0],self.liste_emplacement[i][0].position[1],1,0,self)
        elif self.liste_emplacement[i][1]==2:
            if tour.up_pow==1:
                image_tour="assets/images/medicament_pow1_pow2.png"
            else:
                image_tour="assets/images/medicament_por1_pow2.png"
            tour2=Tour(image_tour,1+(tour.puissance),(tour.rayon),1,self.liste_emplacement[i][0].position[0],self.liste_emplacement[i][0].position[1],tour.up_pow+2,tour.up_por,self)
        else:
            if tour.up_pow==3:
                image_tour="assets/images/medicament_pow1_pow2_pow3.png"
            elif tour.up_pow==2:
                image_tour="assets/images/medicament_por1_pow2_pow3.png"
            elif tour.up_pow==1:
                image_tour="assets/images/medicament_pow1_por2_pow3.png"
            else:
                image_tour="assets/images/medicament_por1_por2_pow3.png"
            tour2=Tour(image_tour,(tour.puissance)+1,(tour.rayon),1,self.liste_emplacement[i][0].position[0],self.liste_emplacement[i][0].position[1],tour.up_pow,tour.up_por,self)
        self.remove(tour)
        self.liste_tour.remove(tour)
        self.add(tour2)
        self.liste_tour.append(tour2)
        self.remove(self.liste_emplacement[i][0])
        self.liste_emplacement[i]=[Sprite("assets/images/medicament_selectionne.png",(self.liste_emplacement[i][0].position[0],self.liste_emplacement[i][0].position[1])),self.liste_emplacement[i][1]+1,0,tour2]
        self.add(self.liste_emplacement[i][0])
        self.selected=None
        self.nb_update-=1
        self.liste_emplacement[i][0].do(Hide())

    def update_range(self,i):
        #on ajoute une tour avec une plus grand portee en supprimant l'ancienne
        tour=self.liste_emplacement[i][3]
        if self.liste_emplacement[i][1]==1:
            tour2=Tour("assets/images/medicament_por1.png",(tour.puissance),50+(tour.rayon),1,self.liste_emplacement[i][0].position[0],self.liste_emplacement[i][0].position[1],0,1,self)
        elif self.liste_emplacement[i][1]==2:
            if tour.up_pow==1:
                image_tour="assets/images/medicament_pow1_por2.png"
            else:
                image_tour="assets/images/medicament_por1_por2.png"
            tour2=Tour(image_tour,1+(tour.puissance),(tour.rayon),1,self.liste_emplacement[i][0].position[0],self.liste_emplacement[i][0].position[1],tour.up_pow,tour.up_por+2,self)
        else:
            if tour.up_pow==3:
                image_tour="assets/images/medicament_pow1_pow2_por3.png"
            elif tour.up_pow==2:
                image_tour="assets/images/medicament_por1_pow2_por3.png"
            elif tour.up_pow==1:
                image_tour="assets/images/medicament_pow1_por2_por3.png"
            else:
                image_tour="assets/images/medicament_por1_por2_por3.png"
            tour2=Tour(image_tour,(tour.puissance),(tour.rayon)+50,1,self.liste_emplacement[i][0].position[0],self.liste_emplacement[i][0].position[1],tour.up_pow,tour.up_por,self)
        self.remove(tour)
        self.liste_tour.remove(tour)
        self.add(tour2)
        self.liste_tour.append(tour2)
        self.remove(self.liste_emplacement[i][0])
        self.liste_emplacement[i]=[Sprite("assets/images/medicament_selectionne.png",(self.liste_emplacement[i][0].position[0],self.liste_emplacement[i][0].position[1])),self.liste_emplacement[i][1]+1,0,tour2]
        self.add(self.liste_emplacement[i][0])
        self.selected=None
        self.nb_update-=1
        self.liste_emplacement[i][0].do(Hide())

    def afficherChemin(self):
        n = len(self.chemin.ronds)
        for i in range(n):
            rond1 = self.chemin.ronds[i]
            self.add(Sprite("assets/images/texturerond.png",rond1.position))
            if i != n-1:
                rond2 = self.chemin.ronds[i+1]
                if (rond2.position[0] - rond1.position[0] > 0):
                    rot = math.atan((rond1.position[1] - rond2.position[1])/(rond2.position[0] - rond1.position[0]))*180/math.pi
                elif (rond2.position[0] - rond1.position[0] < 0):
                    rot = math.atan((rond2.position[1] - rond1.position[1])/(rond1.position[0] - rond2.position[0]))*180/math.pi
                else:
                    rot = 90
                pos = ((rond1.position[0] + rond2.position[0]) // 2, (rond1.position[1] + rond2.position[1]) // 2)
                sca = (math.sqrt((rond1.position[0] - rond2.position[0]) * (rond1.position[0] - rond2.position[0]) + (rond1.position[1] - rond2.position[1]) * (rond1.position[1] - rond2.position[1])) - 64) / 64
                ch = Sprite("assets/images/chemin.png", pos, rot)
                ch.scale_x = sca
                bor1 = Sprite("assets/images/bordrond.png", position = rond1.position)
                bor2 = Sprite("assets/images/bordrond.png", position = rond2.position)
                if (rond1.position[0] < rond2.position[0]):
                    bor1.rotation = rot + 180
                    bor2.rotation = rot
                elif (rond1.position[0] > rond2.position[0]):
                    bor1.rotation = rot
                    bor2.rotation = rot + 180
                elif (rond1.position[1] > rond2.position[1]):
                    bor2.rotation = -90
                else:
                    bor1.rotation = 90
                self.add(bor1)
                self.add(bor2)
                self.add(ch)

    def afficherPlaces(self):
        for place in self.liste_emplacement:
            self.add(place[0])
            self.add(Sprite("assets/images/emplacement_tour.png",(place[0].x,place[0].y-3)))
            place[0].do(Hide())

    def afficherBarreDeVie(self):
        for i in range(self.vie):
            self.liste_vie.append(Sprite("assets/images/coeur1.png",(30+i*30,450), scale = 0.35))
            self.add(self.liste_vie[i])

    def rechargerVie(self):
        if self.vie<3:
            if self.vie%1==0:
                self.liste_vie.append(Sprite("assets/images/coeur1.png",(30+int(self.vie)*30,450), scale = 0.35))
                self.add(self.liste_vie[int(self.vie)])
            else:
                self.remove(self.liste_vie[int(self.vie)])
                self.liste_vie.remove(self.liste_vie[int(self.vie)])
                self.liste_vie.append(Sprite("assets/images/coeur1.png",(30+int(self.vie)*30,450), scale = 0.35))
                if self.vie!=2.5:
                    self.liste_vie.append(Sprite("assets/images/coeur2.png",(30+(int(self.vie)+1)*30,450), scale = 0.35))
                self.add(self.liste_vie[int(self.vie)])
                if self.vie!=2.5:
                    self.add(self.liste_vie[int(self.vie)+1])



    def diminuerVie(self):

        def game_over(dt):
            if not(self.children_names["pause_effect"].are_actions_running()):
                self.unschedule(game_over)
                self.add(menu.GameOver())

        if self.liste_vie!=[]:
            if self.vie%1==0:
                self.remove(self.liste_vie[int(self.vie)-1])
                self.liste_vie[int(self.vie)-1]=Sprite("assets/images/coeur2.png",(30+(int(self.vie)-1)*30,450), scale = 0.35)
                self.add(self.liste_vie[int(self.vie)-1])
                self.vie=self.vie-0.5
            else:
                self.remove(self.liste_vie[int(self.vie)])
                self.liste_vie.remove(self.liste_vie[int(self.vie)])
                self.vie=self.vie-0.5
            if self.vie == 0:
                self.pause_effect(3, 200)
                self.schedule(game_over)

    def ajouterBoutons(self):
        self.add(self.bouton1)
        self.add(self.bouton2)
        self.bouton1.do(Hide())
        self.bouton2.do(Hide())
        self.add(self.bouton1_selected)
        self.add(self.bouton2_selected)
        self.bouton1_selected.do(Hide())
        self.bouton2_selected.do(Hide())

    def on_key_press(self, symbol, modifiers):
        if symbol == key.ESCAPE and not("pause_effect" in self.children_names):
            self.pause_effect(0, 150)
            self.add(menu.PauseMenu())
        return True

    def pause_effect(self, dt, a):
        self.pause_scheduler()
        for o in self.children:
            o[1].pause_scheduler()
        self.add(ColorLayer(0, 0, 0, 0), name = "pause_effect")
        self.children_names["pause_effect"].do(FadeTo(a, dt))


def create_play_scene(lvl):
    background_layer=ColorLayer(150,2,17,255)
    chemin=chargerChemin("lvl" + str(lvl) + ".txt")
    virus_list=[]
    tower_list=[]
    place_list=chargerPlaces("places" + str(lvl) + ".txt")
    main_layer=GameLayer(tower_list,place_list,virus_list,chemin,3,lvl)
    main_layer.afficherChemin()
    main_layer.afficherPlaces()
    main_layer.ajouterBoutons()
    main_layer.afficherBarreDeVie()
    scene=Scene(background_layer,main_layer)
    return(scene)
