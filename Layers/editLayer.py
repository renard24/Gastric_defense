#OLD DO NOT USE

from cocos.layer import Layer, ColorLayer
from cocos.sprite import Sprite
from cocos.scene import Scene
from chemin import Chemin, chargerChemin
from rond import Rond
from cocos.text import Label
import math

CHEMIN_ROND="assets/images/texturerond.png"

class EditLayer(Layer):
    """Pas de rond. Pour l'instant..."""
    is_event_handler = True
    def __init__(self):
        super(EditLayer, self).__init__()
        self.chemin=Chemin()
        self.nbBloc=1
        self.aff_taille = Label(str(self.nbBloc),(50,50), font_size = 28, anchor_x = "center")
        self.add(self.aff_taille)

    def initTestChemin(self):
        if True:
            self.chemin=chargerChemin("fic.txt")
        else:
            print("titi")
            self.chemin.setDepart(50,50)
            self.chemin.addRond(45,1)
            self.chemin.addRond(90,3)
            self.chemin.addRond(30,2)
            self.chemin.sauverChemin("fic.txt")

    def afficheTestChemin(self):
        print "hey"
        for rond in self.chemin.ronds:
            print "oh"
            self.afficheRond(rond)

    def modifTaille(self, nouvTaille):
        if nouvTaille > 0:
            self.nbBloc=nouvTaille
            self.aff_taille.element.text=str(self.nbBloc)

    def afficheRond(self, rond):
        self.add(Sprite(CHEMIN_ROND,rond.position))
        print(rond.position)

    def addRond(self,x,y):
        x=x-self.chemin.ronds[-1].position[0]
        y=y-self.chemin.ronds[-1].position[1]
        angle = 360.0/(2*math.pi)*math.acos((x/math.sqrt(x*x+y*y)))
        if y<0:
            angle=-angle
        print angle
        self.chemin.addRond(angle,self.nbBloc)
        self.afficheRond(self.chemin.ronds[-1])

    def on_mouse_press(self,x,y,buttons, modifiers):
        self.addRond(x,y)

    def on_mouse_scroll(self, x, y, dx, dy):
        self.modifTaille(self.nbBloc+dy)

    def on_key_press(self,  key, modifiers):
        if key == 112:
            self.modifTaille(self.nbBloc+1)
        elif key == 109:
            self.modifTaille(self.nbBloc-1)

def create_test_scene():
    edit=EditLayer()
    edit.initTestChemin()
    print "tata"
    edit.afficheTestChemin()
    print "tutu"
    edit.modifTaille(2)
    print(edit.nbBloc)
    return Scene(edit)
