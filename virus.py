from math import sqrt
from cocos.euclid import Vector2
from cocos.sprite import Sprite
import sys
sys.path.append("./Layers")
sys.path.append("./Model")
from rond import Rond
RAYON_ENNEMI=64

class Virus(Sprite):
    def __init__(self,image,vitesse,rond,sante,tower_list,layer):
        super(Virus, self).__init__(image)

        self.vitesse=vitesse
        self.rond=rond
        self.x=self.rond.getCoordonnee()[0] #calculer un position x a partir du rond de spawn
        self.y=self.rond.getCoordonnee()[1] #calculer un position y a partir du rond de spawn
        self.x_suiv=self.x
        self.y_suiv=self.y
        self.sante=sante
        self.tower_list=tower_list
        self.layer=layer
        self.schedule_interval(self.deplacement,0.01)

    def deplacement(self,dt):
        if ((self.x-self.x_suiv)**2+(self.y-self.y_suiv)**2)<(RAYON_ENNEMI):
            #obtenir de novelles coordonnees et mettre a jour le vecteur de deplacement
            self.rond=self.rond.next
            if self.rond==None:
                self.parent.liste_virus.remove(self)
                self.suppr_virus()   #on supprime le virus s'il a atteint le dernier rond
                self.layer.diminuerVie()
            else:
                self.x_suiv=self.rond.getCoordonnee()[0]
                self.y_suiv=self.rond.getCoordonnee()[1]
            self.dir=Vector2(self.x_suiv-self.x,self.y_suiv-self.y)
            self.dir.normalize()
        self.x=self.x+self.dir.x*dt*self.vitesse
        self.y=self.y+self.dir.y*dt*self.vitesse

    def suppr_virus(self):
        #if self.tower_list!=[]:
            #self.tower_list[0].virus_list.remove(self) #voir si ca marche

            #list_virus=self.tower_list[0].virus_list
            #position=-1
            #curseur=0
            #while position==-1:
            #    print(curseur)
            #    if list_virus[curseur]==self:
            #        position=curseur
            #    else:
            #        curseur+=1
            #for tower in self.tower_list:
            #    tower.virus_list.pop(position)
        self.layer.remove(self)
