from cocos.director import director
from cocos.scene import Scene
from cocos.layer import Layer, MultiplexLayer, ColorLayer
from cocos.menu import Menu, MenuItem, ToggleMenuItem, MultipleMenuItem, fixedPositionMenuLayout, key
from cocos.sprite import Sprite
from cocos.text import Label
from cocos.actions import ScaleBy
from cocos.euclid import Vector2
import pyglet.window
import pyglet.resource
import os.path
from random import randint
from play import create_play_scene
from editLayer import create_edit_scene


class MainMenu(Menu):
    """
    Main menu with the name of the game and 4 buttons : PLAY, EDITOR, OPTIONS and QUIT
    """

    def __init__(self):
        super(MainMenu, self).__init__("Gastric Defense")

        self.font_title['color'] = (0, 0, 0, 0)

        self.create_menu([
            MenuItem('PLAY', self.on_play),
            MenuItem('EDITOR', self.on_editor),
            MenuItem('OPTIONS', self.on_options),
            MenuItem('QUIT', self.on_quit)
        ]) #creation of the buttons

    def on_enter(self): #super(..., self).on_enter() recquired
        super(MainMenu, self).on_enter()
        global menu_layer
        width, height = director.get_window_size()
        menu_layer.add(Sprite("assets/images/titre.png", position = (width//2, 360), scale = .2), name = "title") #title sprite

    def on_exit(self): #super(..., self).on_exit() recquired
        super(MainMenu, self).on_exit()
        global menu_layer
        menu_layer.remove("title")

    def on_play(self):
        global menu_layer
        menu_layer.switch_to(3)

    def on_editor(self):
        director.replace(create_edit_scene()) #create and load the editor scene

    def on_options(self):
        global menu_layer
        menu_layer.switch_to(1) #switch to the options menu layer

    def on_quit(self):

        def switch_to_quitmenu(dt, n, l):
            """
            init the quit question and switch to the quit layer
            """
            if not(l[1].are_actions_running()): #do it only if the pop up have been scaled
                l[1].unschedule(switch_to_quitmenu) #this function is no more call at every frame
                quit_question = Label("Do you want to quit?", (width // 2, 340), font_size = 28, anchor_x = "center") #create the label for the question
                l[0].add(quit_question, name = "quit_question") #display the label
                l[0].switch_to(2) #switch to the quit layer

        global background_layer
        global menu_layer

        width, height = director.get_window_size() #set the window width and height

        if not('quit_window' in menu_layer.children_names): #check if the switch is already called

            background_layer.opacity = 100 #change the background opacity
            for v in background_layer.children:
                v[1].pause_scheduler()
                v[1].opacity = 100

            #create pop up for the quit question
            quit_window = ColorLayer(0, 0, 0, 255, width * 2 // 15, height // 10)
            quit_window.position = ((width - quit_window.width) // 2, (height - quit_window.height) * 3 // 5)
            menu_layer.add(quit_window, name = "quit_window")
            quit_window.transform_anchor = (quit_window.width // 2, quit_window.height // 2) #change the anchor point of the pop up to its center
            quit_window.do(ScaleBy(5, 0.15)) #scale up by 5 the size of the pop up in 0.15 secondes
            quit_window.schedule(switch_to_quitmenu, 2, [menu_layer, quit_window]) #call the function which init the quit layer every frame


class OptionsMenu(Menu):
    """
    Options menu
    """

    def __init__(self):
        super(OptionsMenu, self).__init__('Options')

        self.create_menu([
            MultipleMenuItem('Resolution : ', self.on_resolution, ['640x480', '1024x768', '1366x768', '1600x900', '1920x1080'], get_res()),
            ToggleMenuItem('Fullscreen : ', self.on_fullscreen, False),
            ToggleMenuItem('Show FPS : ', self.on_fps, False),
            MenuItem('BACK', self.on_back)
        ]) #create and display menu items

    def on_resolution(self, size): #TODO Size of the background_layer, /!\mouse on tower
        global background_layer

        if (director.window.fullscreen):
            tmi = self.children[1][1] #tmi = toggle menu item
            tmi.on_key_press(key.ENTER, None)
        if (size == 0):
            w,h = 640,480
        elif (size == 1):
            w,h = 1024,768
        elif (size == 2):
            w,h = 1366,768
        elif (size == 3):
            w,h = 1600,900
        elif (size == 4):
            w,h = 1920,1080

        screen = pyglet.window.get_platform().get_default_display().get_default_screen()
        screen_width = screen.width
        screen_height = screen.height
        director.window.set_size(w,h) #resize the window with pyglet function
        director.window.set_location((screen_width - w) // 2, (screen_height - h) // 2) #set location of the window with pyglet function

    def on_fullscreen(self, b): #TODO position of the mouse after turn on
        director.window.set_fullscreen(b)
        if not(b):
            self.on_resolution(get_res())

    def on_fps(self, b):
        director.set_show_FPS(b)

    def on_back(self):
        global menu_layer
        menu_layer.switch_to(0) #switch to the main menu


class QuitMenu(Menu):#TODO navigation command (right and left)
    """
    Ask the user if he really want to close the game
    """

    def __init__(self):
        super(QuitMenu, self).__init__()

        width, height = director.get_window_size()

        #set the position of the buttons
        pos_yes = [width // 3, (height - self.title_height) * 0.5]
        pos_no = [width * 2 // 3, (height - self.title_height) * 0.5]

        self.create_menu([
            MenuItem('YES', self.on_yes),
            MenuItem('NO', self.on_no)
        ], layout_strategy = fixedPositionMenuLayout([pos_yes, pos_no])) #create and display the yes/no buttons with the position set previously

    def on_yes(self):
        exit() #close the game

    def on_no(self):
        global menu_layer
        global background_layer
        background_layer.opacity = 255
        for v in background_layer.children:
            v[1].resume_scheduler()
            v[1].opacity = 255
        menu_layer.remove("quit_window") #hide the pop up
        menu_layer.remove("quit_question") #hide the question
        menu_layer.switch_to(0) #switch to the main menu

    def on_key_press(self, symbol, modifiers):
        if symbol == key.ESCAPE:
            self.on_quit()
            return True
        elif symbol in (key.ENTER, key.NUM_ENTER):
            self._activate_item()
            return True
        elif symbol in (key.LEFT, key.RIGHT):
            if symbol == key.LEFT:
                new_idx = 0
            elif symbol == key.RIGHT:
                new_idx = 1
            self._select_item(new_idx)
            return True
        else:
            # send the menu item the rest of the keys
            ret = self.children[self.selected_index][1].on_key_press(symbol, modifiers)
            # play sound if key was handled
            if ret and self.activate_sound:
                self.activate_sound.play()
            return ret


class LevelSelection(Menu):

    def __init__(self):
        super(LevelSelection, self).__init__("Select a level")

        i = 1
        l = []
        while check_level(i):
            l.append(MenuItem("Level " + str(i), self.on_level, i))
            i += 1
        l.append(MenuItem("Back", self.on_quit))
        self.create_menu(l)

    def on_level(self, i):
        director.replace(create_play_scene(i))

    def on_quit(self):
        global menu_layer
        menu_layer.switch_to(0)


def check_level(i):
    return os.path.exists("lvl" + str(i) + ".txt")


def get_res():
    width, height = pyglet.window.Window.get_size(director.window)
    if (width == 640):
        return 0
    elif (width == 1024):
        return 1
    elif (width == 1366):
        return 2
    elif (width == 1600):
        return 3
    elif (width == 1920):
        return 4

class Virus(Sprite):

    def __init__(self, image1, image2):
        w, h = director.get_window_size()
        super(Virus, self).__init__(image1, (randint(0, w), randint(0, h)))
        self.dir = Vector2(randint(-w, w), randint(-h, h))
        self.dir.normalize()
        self.sens = randint(0, 1)
        self.speed = randint(120, 200)
        if (self.sens == 0):
            self.sens = -1
        self.image1 = image1
        self.image2 = image2
        self.schedule_interval(self.move, 0.01)
        self.schedule_interval(self.anim, 0.5)

    def move(self, dt):
        w, h = director.get_window_size()
        new_x = self.position[0] + self.dir.x * dt * self.speed
        new_y = self.position[1] + self.dir.y * dt * self.speed
        if ((new_y + self.height//2) > h):
            new_y -= 2*(new_y + self.height//2 - h)
            self.dir[1] *= -1
            if randint(0,1):
                self.sens *= -1
        elif ((new_x + self.width//2) > w):
            new_x -= 2*(new_x + self.width//2 - w)
            self.dir[0] *= -1
            if randint(0,1):
                self.sens *= -1
        elif ((new_x - self.width//2) < 0):
            new_x -= 2*(new_x - self.width//2)
            self.dir[0] *= -1
            if randint(0,1):
                self.sens *= -1
        elif ((new_y - self.height//2) < 0):
            new_y -= 2*(new_y - self.height//2)
            self.dir[1] *= -1
            if randint(0,1):
                self.sens *= -1
        self.position = (new_x, new_y)
        self.rotation += self.sens * dt * 20

    def anim(self, dt):
        if (self.image == pyglet.resource.image(self.image1)):
            self.image = pyglet.resource.image(self.image2)
        else:
            self.image = pyglet.resource.image(self.image1)


def create_menu_scene():
    """
    Create the menu scene and load it
    """

    global menu_layer
    global background_layer

    background_layer = ColorLayer(255, 50, 40, 255)
    background_layer.add(Virus("assets/images/virus10.png", "assets/images/virus11.png"))
    background_layer.add(Virus("assets/images/virus20.png", "assets/images/virus21.png"))
    background_layer.add(Virus("assets/images/virus30.png", "assets/images/virus31.png"))
    background_layer.add(Virus("assets/images/virus10.png", "assets/images/virus11.png"))
    background_layer.add(Virus("assets/images/virus20.png", "assets/images/virus21.png"))
    background_layer.add(Virus("assets/images/virus30.png", "assets/images/virus31.png"))
    background_layer.add(Virus("assets/images/virus10.png", "assets/images/virus11.png"))
    background_layer.add(Virus("assets/images/virus20.png", "assets/images/virus21.png"))
    background_layer.add(Virus("assets/images/virus30.png", "assets/images/virus31.png"))
    background_layer.add(Virus("assets/images/virus10.png", "assets/images/virus11.png"))

    menu_layer = MultiplexLayer(MainMenu(), OptionsMenu(), QuitMenu(), LevelSelection())

    return Scene(background_layer, menu_layer)


######################################################################################################################################


class PauseMenu(Menu):

    def __init__(self):
        super(PauseMenu, self).__init__("PAUSE")

        self.create_menu([
            MenuItem("Resume", self.on_resume),
            MenuItem("Restart", self.on_restart),
            MenuItem("Back to main menu", self.on_back)
        ])

    def on_resume(self):
        self.parent.resume_scheduler()
        for o in self.parent.children:
            o[1].resume_scheduler()
        self.parent.remove("pause_effect")
        self.kill()

    def on_restart(self):
        director.replace(create_play_scene(self.parent.lvl))

    def on_back(self):
        director.replace(create_menu_scene())

    def on_key_press(self, symbol, modifier):
        if symbol == key.ESCAPE:
            self.on_resume()
            return True
        else:
            super(PauseMenu, self).on_key_press(symbol, modifier)


class GameOver(Menu):

    def __init__(self):
        super(GameOver, self).__init__("GAME OVER")

        self.create_menu([
            MenuItem("Restart", self.on_restart),
            MenuItem("Back to main menu", self.on_quit)
        ])

    def on_restart(self):
        director.replace(create_play_scene(self.parent.lvl))

    def on_quit(self):
        director.replace(create_menu_scene())

class Victory(Menu):

    def __init__(self, lvl):
        super(Victory, self).__init__("VICTORY")

        l = []
        if check_level(lvl + 1):
            l.append(MenuItem("Next level", self.on_next, lvl))
        l.append(MenuItem("Back to main menu", self.on_quit))
        self.create_menu(l)

    def on_next(self, lvl):
        director.replace(create_play_scene(lvl + 1))

    def on_quit(self):
        director.replace(create_menu_scene())
