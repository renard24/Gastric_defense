#!/usr/bin/python3

from cocos.director import director
from menu import create_menu_scene


def main():
    director.init(caption="Gastric_Defense")
    director.run(create_menu_scene())

if __name__=="__main__":
    main()
