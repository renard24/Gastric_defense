#from rond import
from cocos.euclid import Vector2
from cocos.sprite import Sprite
from cocos.actions.interval_actions import Blink
from math import sqrt
from cocos.collision_model import CollisionManager

degats=Blink(3,0.5)

class Tour(Sprite):
    def __init__(self,image,puissance,rayon,cadence,x,y,up_pow,up_por,layer):
        super(Tour, self).__init__(image)

        self.puissance=puissance
        self.x=x
        self.y=y
        self.rayon=rayon
        self.cadence=cadence
        self.layer=layer
        self.up_pow=up_pow
        self.up_por=up_por
        self.virus_list=layer.liste_virus
        self.schedule_interval(self.detection,(self.cadence))

    def detection(self,dt):
        """Cherche dans la liste des virus a l'ecran celui qui est le plus proche de la tour et lui tire dessus.
         Si aucun virus n'est present, la tour ne tire pas."""
        verif_tir=False
        dist_cible=self.rayon
        l=len(self.virus_list)
        for i in range(l):   #parcours de la liste des virus
            if sqrt((self.x-self.virus_list[i].x)**2+(self.y-self.virus_list[i].y)**2)<dist_cible:
                cible=i      #si un virus est cible, on l'obtient par son rang dans la liste des virus
                dist_cible=sqrt((self.x-self.virus_list[i].x)**2+(self.y-self.virus_list[i].y)**2)
                verif_tir=True
        if verif_tir==True:
            self.tir(cible)  #declanche le tir si un virus est cible

    def tir(self,cible):
        """Inflige des degats au virus cible selon la puissance de la tour. Si les PV
        du virus tombent a 0, le virus est supprime de la liste et de l'ecran"""
        self.virus_list[cible].sante=self.virus_list[cible].sante-self.puissance   #les degats sont infliges au virus cible
        self.virus_list[cible].do(degats)     #animation signifiant la prise de degats d'un virus
        tir=Boulet("assets/images/anticorps.png",3200,self.virus_list[cible],self.x,self.y,self.rayon,self.layer)  #fait apparaitre un tri
        self.layer.add(tir)
        if self.virus_list[cible].sante<=0:   #si le virus n'a plus de PV, on supprime toute trace de lui
            self.layer.remove(self.virus_list[cible])
            self.virus_list.pop(cible)



class Boulet(Sprite):
    def __init__(self,image,vitesse,virus,x,y,portee,layer):
        super(Boulet, self).__init__(image)

        self.x=x
        self.y=y
        self.x_tour=x
        self.y_tour=y
        self.virus=virus
        self.vitesse=vitesse
        self.portee=portee-50
        self.layer=layer
        self.dir=Vector2(self.virus.x-self.x,self.virus.y-self.y)
        self.dir.normalize()
        self.schedule_interval(self.deplacement,0.01)

    def deplacement(self,dt):
        self.x=self.x+self.dir.x*dt*self.vitesse
        self.y=self.y+self.dir.y*dt*self.vitesse
        if self.x<self.x_tour-self.portee or self.x>self.x_tour+self.portee or self.y<self.y_tour-self.portee or self.y>self.y_tour+self.portee:
            self.layer.remove(self)
