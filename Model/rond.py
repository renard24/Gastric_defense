import random
RAYON=64 #Rayon du rond
RAYON_ENNEMI=64 #Rayon dans lequel peut etre l'ennemi (a changer)

class Rond(object):
    """Un rond."""

    def __init__(self, x, y, angle):
        super(Rond, self).__init__()
        self.position=(x,y)
        self.next=None
        self.angle=angle

    def getCoordonnee(self):
        x=random.randint(RAYON_ENNEMI*-1,RAYON_ENNEMI)
        y=random.randint(RAYON_ENNEMI*-1,RAYON_ENNEMI)
        while (x*x+y*y) > (RAYON_ENNEMI/8)**2:
            x=random.randint(RAYON_ENNEMI*-1,RAYON_ENNEMI)
            y=random.randint(RAYON_ENNEMI*-1,RAYON_ENNEMI)
        x+=self.position[0]
        y+=self.position[1]
        return (x,y)
