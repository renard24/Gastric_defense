from rond import Rond
from cocos.sprite import Sprite
import math

TAILLE_BLOC=64
ANGLE_MAX=150
class Chemin(object):
    """Plein de ronds."""
    def __init__(self):
        super(Chemin, self).__init__()
        self.ronds=[]
        self.depart=(0,0)

    def setDepart(self,x,y):
        if self.ronds==[]:
            self.depart=(x,y)

    def addRond(self, angle, nbBloc):
        ok=False
        if self.ronds==[]:
            ok=True
            x=self.depart[0]
            y=self.depart[1]
        elif self.ronds[-1].angle - angle < ANGLE_MAX or self.ronds[-1].angle - angle > 360- ANGLE_MAX:
            ok=True
            x=self.ronds[-1].position[0]
            y=self.ronds[-1].position[1]
        if ok:
            newX=x+math.cos(angle/360.0*(2*math.pi))*TAILLE_BLOC*nbBloc
            newY=y+math.sin(angle/360.0*(2*math.pi))*TAILLE_BLOC*nbBloc
            rond=Rond(newX,newY,angle)
            self.addRond2(rond)

    def addRond2(self, rond):
        if self.ronds!=[]:
            self.ronds[-1].next=rond
        self.ronds.append(rond)

    def sauverChemin(self, fic):
        f=open(fic,"w")
        if self.ronds != []:
            buf=str(self.depart[0])+" "+str(self.depart[1])
            f.write(buf)
            for rond in self.ronds:
                buf="\n"+str(rond.position[0])+" "+str(rond.position[1])+" "+str(rond.angle)
                f.write(buf)
        f.close()

    def afficheRond(self, rond):
        self.add(Sprite(CHEMIN_ROND,rond.position))

def chargerChemin(fic):
    f=open(fic,"r")
    content=f.read()
    content=content.split("\n")
    chemin=Chemin()
    l=content[0].split(" ")
    chemin.setDepart(l[0],l[1])
    del content[0]
    for l in content:
        l=l.split(" ")
        if l[0]!="":
            rond=Rond(float(l[0]),float(l[1]),float(l[2]))
            chemin.addRond2(rond)
    f.close
    return chemin

def chargerPlaces(fic):
    f=open(fic,"r")
    content=f.read()
    content=content.split("\n")
    places=[]
    l=content[0].split(" ")
    for l in content:
        l=l.split(" ")
        if l[0]!="":
            place=[Sprite("assets/Case_test.png",(float(l[0]),float(l[1]))),0,0]
            places.append(place)
    f.close()
    return places
