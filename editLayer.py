from cocos.layer import Layer, ColorLayer
from cocos.sprite import Sprite
from cocos.scene import Scene
from cocos.director import director
from chemin import Chemin, chargerChemin
from rond import Rond
from cocos.text import Label
import math
import menu

CHEMIN_ROND="assets/images/texturerond.png"

class EditLayer(Layer):
    """Pas de rond. Pour l'instant..."""
    is_event_handler = True
    def __init__(self):
        super(EditLayer, self).__init__()
        self.chemin=Chemin()
        self.nbBloc=1
        self.aff_taille = Label(str(self.nbBloc),(50,50), font_size = 28, anchor_x = "center")
        self.tips=Label("q : quitter, s : sauver et quitter, p : plus, m : moins",(50,450), font_size = 10, anchor_x = "left")
        self.add(self.aff_taille)
        self.add(self.tips)

    def initTestChemin(self):
        if True:
            self.chemin=chargerChemin("fic.txt")
        else:
            self.chemin.setDepart(50,50)
            self.chemin.addRond(45,1)
            self.chemin.addRond(90,3)
            self.chemin.addRond(30,2)


    def afficheTestChemin(self):
        for rond in self.chemin.ronds:
            self.afficheRond(rond)

    def modifTaille(self, nouvTaille):
        if nouvTaille > 0:
            self.nbBloc=nouvTaille
            self.aff_taille.element.text=str(self.nbBloc)

    def afficheRond(self, rond):
        self.add(Sprite(CHEMIN_ROND,rond.position))
        print(rond.position)

    def addRond(self,x,y):
        x=x-self.chemin.ronds[-1].position[0]
        y=y-self.chemin.ronds[-1].position[1]
        angle = 360.0/(2*math.pi)*math.acos((x/math.sqrt(x*x+y*y)))
        if y<0:
            angle=-angle
        print(angle)
        self.chemin.addRond(angle,self.nbBloc)
        self.afficheRond(self.chemin.ronds[-1])

    def on_mouse_press(self,x,y,buttons, modifiers):
        self.addRond(x,y)

    def on_mouse_scroll(self, x, y, dx, dy):
        self.modifTaille(self.nbBloc+dy)

    def on_save(self):
        self.chemin.sauverChemin("fic.txt")
        self.on_quit()

    def on_quit(self):
        director.replace(menu.create_menu_scene())

    def on_key_press(self,  key, modifiers):
        print(key)
        if key == 112: #p
            self.modifTaille(self.nbBloc+1)
        elif key == 109: #m
            self.modifTaille(self.nbBloc-1)
        elif key == 113: #q
            print("quit")
            self.on_quit()
        elif key == 115: #s
            self.on_save()

def create_edit_scene():
    edit=EditLayer()
    edit.initTestChemin()
    edit.afficheTestChemin()
    return Scene(edit)
